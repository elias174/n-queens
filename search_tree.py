from treelib import Node, Tree

import codecs


def export_to_dot(tree, filename, shape='circle', graph='strict digraph'):
    """Exports the tree in the dot format of the graphviz software"""

    nodes, connections = [], []
    if tree.nodes:

        for n in tree.expand_tree(mode=tree.WIDTH):
            nid = tree[n].identifier
            state = '"' + str(nid) + '"' + ' [label="' + str(tree[
                n].tag) + '", shape=' + shape + ']'
            nodes.append(state)

            for c in tree.children(nid):
                cid = c.identifier

                connections.append('"' + str(nid) + '"' + ' -> ' + '"' + str(cid) + '"')

    # write nodes and connections to dot format
    with codecs.open(filename, 'w', 'utf-8') as f:
        f.write(graph + ' tree {\n')
        for n in nodes:
            f.write('\t' + n + '\n')

        f.write('\n')
        for c in connections:
            f.write('\t' + c + '\n')

        f.write('}')

class NotPointFoundError(Exception):
    pass

class NotSolutionsError(Exception):
    pass


class Move(object):
    def __init__(self, n_size, coord, memory):
        self.n_size = n_size
        self.coord = coord
        self.memory = memory

    @staticmethod
    def calculate_bias(x, y):
        return [y - x, y + x]

    @staticmethod
    def correct_point(i, S):
        for p in S:
            bias = Move.calculate_bias(p[0], p[1])
            if (i[1] == p[1]
                or i[1] - i[0] == bias[0] or i[1] + i[0] == bias[1]):
                return False
        return True

    @staticmethod
    def find_points(n_size, i, memory):
        points = []
        index = i[1]
        while index < n_size:
            if Move.correct_point([i[0], index], memory):
                new_memory = [] + memory
                points.append(Move(n_size, [i[0], index], new_memory))
            index += 1
        return points

    def next_moves(self):
        if self.coord[0] + 1 > self.n_size:
            raise NotPointFoundError
        coord_next = [self.coord[0] + 1, 0]
        next_memory = self.memory + [self.coord]
        moves = Move.find_points(self.n_size, coord_next, next_memory)

        return moves

    def __repr__(self):
        return str(self.coord)

    def __str__(self):
        return str(self.coord)



class NQueens(object):
    def __init__(self, n_size):
        self.n_size = n_size
        self.tree = Tree()
        self.dfs_solve = None
        self.root = 'Game of %d Size' % self.n_size
        self.tree.create_node(identifier=self.root)
        self.generate_first_childs()
        for child in self.tree.children(self.root):
            self.build_tree(child.identifier)

    def generate_first_childs(self):
        row = 0
        for column in range(self.n_size):
            self.tree.create_node(
                identifier=Move(self.n_size, [row, column], []), parent=self.root)

    def build_tree(self, root):
        if len(root.next_moves()) < 1:
            return
        for move in root.next_moves():
            self.tree.create_node(identifier=move, parent=root)
            self.build_tree(move)

    def get_initial_node(self, identifier):
        for child in self.tree.children(self.root):
            if child.identifier.__str__() == identifier:
                return child.identifier
        raise NotPointFoundError

    def dfs(self, L):
        if len(L) < 1:
            return
        n = L[0]
        if n.coord[0] == self.n_size - 1:
            self.dfs_solve = n
            return
        L.pop(0)
        for child in self.tree.children(n):
            L.append(child.identifier)
        self.dfs(L)

    @staticmethod
    def get_solution_dfs(initial, n_size):
        game = NQueens(n_size)
        node_initial = game.get_initial_node(initial)
        childs = game.tree.children(node_initial)
        L = []
        for child in childs:
            L.append(child.identifier)
        game.dfs(L)
        if game.dfs_solve:
            return game.dfs_solve.memory + [game.dfs_solve.coord]
        raise NotSolutionsError('No se encontro Solucion')






def get_moves():
    # root_move = Move(6,[0,2], [])
    # next_move = root_move.next_moves()[0]
    # next_move_bro = root_move.next_moves()[1]
    # print next_move.next_moves()
    # print next_move_bro.next_moves()

    # next_move2 = next_move[2].next_moves()
    # # next_move3 = next_move2[1].next_moves()
    # # next_move4 = next_move3[1].next_moves()
    # # next_move5 = next_move4[0].next_moves()
    # # print next_move[0].memory
    # print next_move2
    solve = NQueens.get_solution_dfs('[0, 0]', 4)
    print solve
    print solve.memory
    #print game.get_initial_node('[0, 0]').identifier.memory
    #export_to_dot(game.tree, 'tree.dot')

# get_moves()
# def main():
#     game = NQueens(4)
#     game.build_tree()

# def inorder(root):
#     leafs = tree.children(root)
#     print root
#     if len(leafs) < 1:
#         return
#     for leaf in leafs:
#         inorder(leaf.identifier)
#
#
# inorder(tree.root)

# S = [[0, 1], [1, 3], [2, 0]]
# n = 3
# i = [3, 0]

# def find_point(S, i, n):
#     for p in S:
#         bias = calculate_bias(p[0], p[1])
#         while i[1] == p[1] or i[1] - i[0] == bias[0] or i[1] + i[0] == bias[1]:
#             i[1] += 1
#             if i[1] > n:
#                 print i[1]
#                 raise NotPointFoundError
#     return i

# print find_point(S, i, n)