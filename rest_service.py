from flask import Flask
from flask.ext.restful import Api, Resource, reqparse, fields, marshal, abort

from search_tree import NQueens, NotSolutionsError

app = Flask(__name__)
api = Api(app)

class Game(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('col', type=int)
        self.parser.add_argument('row', type=int)
        self.parser.add_argument('size', type=int)
        super(Game, self).__init__()

    def get(self):
        args = self.parser.parse_args()

        col = args.get('col', -1)
        row = args.get('row', -1)
        size = args.get('size')

        if not (col >= 0 and row >= 0 and size):
            abort(400, message="Not Enough Arguments")

        if col >= size or row >= size:
            abort(400, message="Bad Dimentions")

        initial_move = [row, col]
        try:
            solve = NQueens.get_solution_dfs(str(initial_move), size)
        except NotSolutionsError:
            abort(400, 'No Solutions Founded')
        solutions = solve
        result = []
        for solution in solutions:
            result.append({
                'row': solution[0],
                'col': solution[1]}
            )
        return {'solution': result}

api.add_resource(Game, '/')

if __name__ == '__main__':
    app.run(debug=True)